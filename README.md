# README #


### What is this repository for? ###

* simpleNames is a small contact manager an app for simpleTechs
* Version 1.0

### About this repo ###

* Code implementation started on july 4th 2017 - and finished on july 5th 2017 - implementation duration (2 days)
* the project uses Alamofire for api consumption, official CoreData library and models, Views are implemented using Storyboards only, no explicit code-based constraints are added
* UI adaptive on all different devices iPads and iPhones (Uses both UINavigationController and UISplitViewController)
* for ease of usage the add view is attached to the UITableView header
* App transport security is disabled for testing purposes as the api doesn't provide encrypted https endpoints
* consumes an api of names and stores it in local coredata storage
* uses Pull down to refresh theme to retrieve server-side contacts supported with UIRefreshControl
* provide the ability to delete items via sliding left on a contact row
* provide validation of data (email) by UI RegEx in-view validation and coredata model based validation
* informative error, success and status reporting
* one UI test implemented
* the code is documented with some parts explained
* please install all pods before running the project

### Any Questions are welcomed ###
* Momen Shataly (shataly@gmail.com)