//
//  RegX.swift
//  simpleContacts
//
//  Created by Momen on 7/5/17.
//  Copyright © 2017 simpleTechs. All rights reserved.
//

import UIKit

class RegX {

    
    public static func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with:candidate)
    }
    
}
