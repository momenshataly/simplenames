//
//  DetailViewController.swift
//  simpleContacts
//
//  Created by Momen on 7/4/17.
//  Copyright © 2017 simpleTechs. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_mail: UILabel!


    func configureView() {
        // Update the user interface for the detail item.
        // if let to avoid crashes when the data is passed but the view still not initialized
        if let detail = detailItem {
            if let label = lbl_name {
                label.text = detail.name!
                
            }
  
            if let label2 = lbl_mail{
                
                label2.text = detail.mail!
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: Contact? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    // ==== lightContent statusbar
    override var preferredStatusBarStyle: UIStatusBarStyle{
        
        return .lightContent
    }

}

