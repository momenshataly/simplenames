//
//  MasterViewController.swift
//  simpleContacts
//
//  Created by Momen on 7/4/17.
//  Copyright © 2017 simpleTechs. All rights reserved.
//

import UIKit
import CoreData

class MasterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate,UITextFieldDelegate {

    var detailViewController: DetailViewController? = nil
    var managedObjectContext: NSManagedObjectContext? = nil
    
    @IBOutlet var txt_Name: UITextField!
    @IBOutlet var txt_Mail: UITextField!
    
    @IBOutlet var headerView: UIView!
    var addId : Int64!
   @IBOutlet var tableView : UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let split = splitViewController { //wrap the splitviewcontroller for ipad
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo"))

        // add a UIRefreshControl for the uitableview to indicate the retrieval of server-side contacts
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.addTarget(self, action: #selector(self.refreshFromServer), for: .valueChanged)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        //deselect selected rows when the viewWillAppear only if the splitviewcontroller is collapsed
        if splitViewController!.isCollapsed{
            if self.tableView!.indexPathForSelectedRow != nil {
            self.tableView!.deselectRow(at: self.tableView!.indexPathForSelectedRow!, animated: true)
            }
        }
        super.viewWillAppear(animated)
        //set the headerview of the tableview with the headerview that is a subview in built in the viewcontroller storyboard, aligning both tops in a constraint will result in a fixed on the top headerview without breaking the IB constraints
        self.tableView.tableHeaderView = self.headerView
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //calls the ServerManager singleton class to retrieve the data from the server
    func refreshFromServer(){
        
        //hide the keywboard
        hideKeyWindow()
        //begin refreshing the refreshControl
        self.tableView.refreshControl?.beginRefreshing()
        AlertManager.warning(message: "Fetching server data")
        
        //call the method with a completion block
        ServerManager.sharedManager.getContacts { (data: [Dictionary<String,Any>]!) in
            
            
           // iterate each object inside the data array
            for obj in data{
 
            do {
             
                // get the context.
                let context = self.fetchedResultsController.managedObjectContext
                
                // create a fetch request by an id pred to see if there are any duplicates by id
                let deleteDuplicateRequest : NSFetchRequest<Contact> = Contact.fetchRequest()
                let pred = NSPredicate(format: "%K == %d", "id" ,obj["id"] as! Int64 )
                deleteDuplicateRequest.predicate = pred
                //fetch to get duplicate results
                let result = try context.fetch(deleteDuplicateRequest)

                
                if result.count > 0 {
                    //iterate on the results
                    for obj in result{
                        //delete the duplicate result
                        context.delete(obj)
                    }
                }
                
                //create a new Contact in context
                let newContact = Contact(context: context)
                //set data of the context based on the server dictionary
                newContact.name = obj["name"] as? String
                newContact.mail = obj["mail"] as? String
                newContact.id = (obj["id"] as? Int64)!
                
                //try saving the context
                    try context.save()
                    
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    

                    let nserror = error as NSError
                 //fatal error in saving data showing error msg
                    AlertManager.error(message: "Unresolved error \(nserror), \(nserror.userInfo)")
                    self.tableView.refreshControl?.endRefreshing()
                    return;
                }
                
                
                
            }
            //end refreshing the UIRefreshControl
            self.tableView.refreshControl?.endRefreshing()
            AlertManager.success(message: "Server data fetched successfully")
        }
    }
    
    
// adds a local object from the add view
   @IBAction func insertNewObject(_ sender: Any) {
    
    
    //trim white spaces and newlines
    if ((txt_Name.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! || (txt_Mail.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)!){
    AlertManager.warning(message: "Please fill the empty field")
    return
    }
    if !RegX.validateEmail(candidate: (txt_Mail.text?.trimmingCharacters(in: .whitespacesAndNewlines))!){
        AlertManager.warning(message: "Invalid email")
        txt_Mail.becomeFirstResponder()
        txt_Mail.selectAll(nil)
        return
    }
        let context = self.fetchedResultsController.managedObjectContext
        let newContact = Contact(context: context)
             newContact.name = txt_Name.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
             newContact.mail = txt_Mail.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
             newContact.id = self.addId
        
        // Save the context.
        do {
            try context.save()
            txt_Name.text = ""
            txt_Mail.text = ""
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nserror = error as NSError
            AlertManager.error(message: "Unresolved error \(nserror), \(nserror.userInfo)")
            return
        }
        AlertManager.success(message: "New contact added successfully")

    }

    
    // ==== lightContent statusbar
    override var preferredStatusBarStyle: UIStatusBarStyle{
        
        return .lightContent
    }
    
    func hideKeyWindow(){
        //hides keyboard
        UIApplication.shared.keyWindow?.endEditing(true)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
            let object = fetchedResultsController.object(at: indexPath)
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

     func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections!.count
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

       let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath)
        let contact = fetchedResultsController.object(at: indexPath)
        configureCell(cell, withContact: contact)
        
        return cell
    }

     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        
        return true
    }


    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        

        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "\u{1F5D1}\n\nTrash", handler: { (action, indexPath) in
            let context = self.fetchedResultsController.managedObjectContext
            context.delete(self.fetchedResultsController.object(at: indexPath))
            
            
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                AlertManager.error(message: "Unresolved error \(nserror), \(nserror.userInfo)")
                return
            }
            
            AlertManager.success(message: "Contact deleted successfully")

        })
        

        
        return [deleteAction]
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Gespeicherte Kontakte"
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if view is UITableViewHeaderFooterView {
            
            (view as! UITableViewHeaderFooterView).textLabel?.text = (view as! UITableViewHeaderFooterView).textLabel?.text?.localizedCapitalized
        }
    }

    func configureCell(_ cell: UITableViewCell, withContact contact: Contact) {
        cell.textLabel!.text = contact.name!.description
        cell.detailTextLabel!.text = contact.mail!.description
    }

    // MARK: - Fetched results controller

    var fetchedResultsController: NSFetchedResultsController<Contact> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        AlertManager.warning(message: "Fetching local contacts")

        let fetchRequest: NSFetchRequest<Contact> = Contact.fetchRequest()
        
        // Set the batch size to a suitable number.
        //fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: false)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: nil)

        
        //aFetchedResultsController.sections?.append(NSFetchedResultsSectionInfo()
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
       
        
        do {
            try _fetchedResultsController!.performFetch()

        } catch {
             // Replace this implementation with code to handle the error appropriately.
             // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
             let nserror = error as NSError
            AlertManager.error(message: "Unresolved error \(nserror), \(nserror.userInfo)")
            return _fetchedResultsController!
        }
        
       // print("\(_fetchedResultsController?.fetchedObjects?.count)")
         self.addId = ((_fetchedResultsController!.fetchedObjects?.count)! > 0) ? ((_fetchedResultsController!.fetchedObjects?.last?.id)! + 1) : 0
        AlertManager.success(message: "Local contacts fetched successfully")
        return _fetchedResultsController!
    }    
    var _fetchedResultsController: NSFetchedResultsController<Contact>? = nil

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
   
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
       // let RefSectionIndex = sectionIndex + 1
        
        switch type {
            case .insert:
                tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
            case .delete:
                tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
            default:
                return
        }
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
       
        switch type {
            case .insert:
                //let RefNewIndexPath  = reformTableHeaderSecionAt(indexPath: newIndexPath!)
                tableView.insertRows(at: [newIndexPath!], with: .fade)
            case .delete:
                //let  RefIndexPath  = reformTableHeaderSecionAt(indexPath: indexPath!)
                tableView.deleteRows(at: [indexPath!], with: .fade)
            case .update:
                //let  RefIndexPath  = reformTableHeaderSecionAt(indexPath: indexPath!)
                configureCell(tableView.cellForRow(at: indexPath!)!, withContact: anObject as! Contact)
            case .move:
             //  let RefNewIndexPath  = reformTableHeaderSecionAt(indexPath: newIndexPath!)
             //   let  RefIndexPath  = reformTableHeaderSecionAt(indexPath: indexPath!)
                configureCell(tableView.cellForRow(at: indexPath!)!, withContact: anObject as! Contact)
                tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }

    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        tableView.endUpdates()
        
    }
    
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        hideKeyWindow()
        return true
    }
    

}

