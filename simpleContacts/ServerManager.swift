//
//  ServerManager.swift
//  nytimes
//
//  Created by Momen on 12/7/16.
//  Copyright © 2016 Momen. All rights reserved.
//

import UIKit
import Alamofire
import CoreData


/**
 predefined aka "macros"
 
 API_URL : String : the uniform resource locator of the webservice endpoint to retrieve the contacts
 */
let API_URL = "http://www.filltext.com/?rows=10&name=%7BfirstName%7D~%7BlastName%7D&mail=%7Bemail%7D&id=%7Bindex%7D&pretty=true"


class ServerManager {

    /** private init to prevent initializing on runtime : the singleton design pattern
        one and only one object is created at runtime
     */
    private init(){

    }
    /**
     shared singleton property
     */
    static let sharedManager = ServerManager();
    
    
    /**
     function getContacts
     request the contacts from the webservice and retrieve it as array of Dictionary
    
     
     Parameters:
     callBack : block that inputs the result when finished
     */
    
    internal func getContacts(callBack: @escaping ([Dictionary<String,Any>]!) -> Void){
        
        
        Alamofire.request(API_URL, method: .get ,parameters: nil, encoding: URLEncoding.default)
            .validate()
            .responseJSON { (response) -> Void  in
                
                guard response.result.isSuccess else {
                    print("Error while fetching data: \(String(describing: response.result.error))")
                    callBack(nil)
                    return
                }
                //if any results from query
                guard let responseJSON : Array<Dictionary<String,Any>> = response.result.value as? Array<Dictionary<String,Any>> else{
                    print("Malformed data received from service")
                    callBack(nil)
                    return
                }
                
                var resultArray:[Dictionary<String,Any>]! = []
                
                for obj in responseJSON{

                    resultArray.append(obj)

                }
                callBack(resultArray)
        }
    }
    
   

}
