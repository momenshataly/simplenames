//
//  AlertManager.swift
//  simpleContacts
//
//  Created by Momen on 7/5/17.
//  Copyright © 2017 simpleTechs. All rights reserved.
//

import UIKit
import CRToast
import SwiftHEXColors
import CoreFoundation
//Util class to show alerts
class AlertManager {

    
    public static func success(message: String!){
        
        
        let options: [String:Any]! = [
            kCRToastTextKey : message,
            //kCRToastTextAlignmentKey : NSTextAlignment.center,
            kCRToastBackgroundColorKey : UIColor(hexString: "4FCE47")!,
            kCRToastStatusBarStyleKey : NSNumber(value: UIStatusBarStyle.lightContent.rawValue)
            //kCRToastAnimationInTypeKey : CRToastAnimationType.gravity.rawValue,
            //kCRToastAnimationOutTypeKey : CRToastAnimationType.gravity.rawValue,
            //kCRToastAnimationInDirectionKey : CRToastAnimationDirection.top.rawValue,
            //kCRToastAnimationOutDirectionKey : CRToastAnimationDirection.top.rawValue,
            
        ]
        
      CRToastManager.showNotification(options: options) {
        
        }
    }
    
    
    public static func error(message: String!){
        
        
        let options: [String:Any]! = [
            kCRToastTextKey : message,
            //kCRToastTextAlignmentKey : NSTextAlignment.center,
            kCRToastBackgroundColorKey : UIColor.red,
            kCRToastStatusBarStyleKey : NSNumber(value: UIStatusBarStyle.lightContent.rawValue)
            //kCRToastAnimationInTypeKey : CRToastAnimationType.gravity.rawValue,
            //kCRToastAnimationOutTypeKey : CRToastAnimationType.gravity.rawValue,
            //kCRToastAnimationInDirectionKey : CRToastAnimationDirection.top.rawValue,
            //kCRToastAnimationOutDirectionKey : CRToastAnimationDirection.top.rawValue,
            
        ]
        
        CRToastManager.showNotification(options: options) {
            
        }
    }
    
    
    public static func warning(message: String!){
        
        
        let options: [String:Any]! = [
            kCRToastTextKey : message,
            //kCRToastTextAlignmentKey : NSTextAlignment.center,
            kCRToastBackgroundColorKey : UIColor.orange,
            kCRToastStatusBarStyleKey : NSNumber(value: UIStatusBarStyle.lightContent.rawValue)
            //kCRToastAnimationInTypeKey : CRToastAnimationType.gravity.rawValue,
            //kCRToastAnimationOutTypeKey : CRToastAnimationType.gravity.rawValue,
            //kCRToastAnimationInDirectionKey : CRToastAnimationDirection.top.rawValue,
            //kCRToastAnimationOutDirectionKey : CRToastAnimationDirection.top.rawValue,
            
        ]
        
        CRToastManager.showNotification(options: options) {
            
        }
        
    }
    
}
