//
//  MSSplitViewController.swift
//  simpleContacts
//
//  Created by Momen on 7/5/17.
//  Copyright © 2017 simpleTechs. All rights reserved.
//

import UIKit

class MSSplitViewController: UISplitViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // ==== lightContent statusbar
    override var preferredStatusBarStyle: UIStatusBarStyle{
        
        return .lightContent
    }

}
